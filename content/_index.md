---
title: Archivos Lúdicos
---

<div style="text-align: center">
<img src ="https://gitlab.com/JamepDev/archivosludicos/-/raw/master/content/index.png">

Bienvenido a este proyecto open source hecho para recopilar y compartir información sobre videojuegos.

</div>

***
Esta página hace uso de la [licencia MIT](https://gitlab.com/JamepDev/archivosludicos/-/blob/master/LICENSE)
